


---
### 《Stata直播丨直击面板数据模型 ——公开课》

---
> &#x23E9; 在线[观看](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38)：      
> 点击 <https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38>       
> 注册 [连享会直播间](http://lianxh.duanshu.com) 账号后即可免费观看。时长：1小时40分钟。
---

&emsp;

### 嘉宾简介


![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连玉君工作照100.JPG)  

**[连玉君](http://lingnan.sysu.edu.cn/node/151)** ，经济学博士，中山大学岭南学院副教授，博士生导师。已在 **China Economic Review**，**经济研究**，**管理世界**，**金融研究** 等期刊发表论文 60 余篇。连玉君老师团队一直积极分享 Stata 应用经验，创办了公众号「Stata连享会 (StataChina)」，开设了 [[Stata连享会-简书]](https://www.jianshu.com/u/69a30474ef33)，[[Stata连享会-知乎]](https://www.zhihu.com/people/arlionn) 两个专栏，累计阅读量超过 200 万人次。

&emsp; 

### 课程概览

> &#x1F4D9; [直播课展示](https://gitee.com/arlionn/Live)  &rarr; 包含部分课程课件和参考资料

本课程是 [[连玉君-动态面板数据模型 (2.2小时)]](https://efves.duanshu.com/#/brief/course/3c3ac06108594577a6e3112323d93f3e) 和 [[连玉君-我的甲壳虫-经典论文精讲 (6小时)]](https://lianxh.duanshu.com/#/brief/course/c3f79a0395a84d2f868d3502c348eafc) 的先导课程，以介绍模型设定思路为主，实操部分请下载 「**Lian_Panel.rar**」压缩包自行演练。

课程中提及的各类模型的实操和综合应用会在 [[连玉君-我的甲壳虫(6小时)]](https://lianxh.duanshu.com/#/brief/course/c3f79a0395a84d2f868d3502c348eafc) 课程中详细讲解。

此外，若想加强研究设计能力，可以观看 [[连玉君-我的特斯拉-实证研究设计]](https://lianxh.duanshu.com/#/course/5ae82756cc1b478c872a63cbca4f0a5e) (2小时，包含 5 篇论文的重现资料，已经有 1000+ 人学习了该课程)。


&emsp;

#### 为何学习面板数据模型？

目前的实证分析中，基本上都是以「面板数据」为分析对象。好处很明显，一方面，随着样本量的增加，我们的统计推断会更加稳健；另一方面，面板数据同时提供了时序和截面的信息，使得我们既可以分析个体之间的截面差异，也可以分析他们时序动态变化。最为重要的是，使用面板数据还是缓解内生性问题的一个主要方法——我们可以控制那些不可观测的固定效应的影响。

本课程对面板数据模型进行整体简介，包括：固定效应模型 (FE)，随机效应模型 (RE)，二维固定效应模型 (Twoway FE)，聚类调整后的标准误，动态面板和面板门槛模型等。

> ![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会-直击面板数据二维码.png)    
> 扫码观看视频


### 课程主题

本次直播主要包括如下几个主题：

- 简介：面板数据结构、优势和挑战
- 什么是「固定效应」？辛普森悖论
- 一维和二维固定效应模型
- 估计方法对比分析：POLS，DVLS，Within-FE
- 聚类标准误：一维聚类和多维聚类
- 实证分析中的主要陷阱
- 动态面板和面板门限模型简介

### 获取课程电子包

你可以点击主页右上方的【克隆/下载&rarr;下载zip】，以便下载本仓库的压缩包；也可以申请一个 [码云](https://gitee.com) 账号，然后点击本项目右上角的【Fork】按钮，这样就可以直接把这个仓库「叉」到你的账号下了，随后我这边更新后，你只需要同步一下就可以看到所有的文件了。


> **实操：Stata dofiles/Data/Papers:** 

下载地址：<https://pan.baidu.com/s/1Ri38Xyz_TnFzLNYgsOes4w> (百度云盘，无解压码)。

&emsp; 

&#x1F34E; &#x1F34F;


&emsp;

&emsp;

## 相关课程

> **连享会-直播课** 上线了！         
>  <http://lianxh.duanshu.com>  

> **免费公开课：**
> - [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) - 连玉君，时长：1小时40分钟
> - [Stata 33 讲](https://lianxh.duanshu.com/#/brief/course/b22b17ee02c24015ae759478697df2a0) - 连玉君, 每讲 15 分钟. 
> - 部分直播课 [课程资料下载](https://gitee.com/arlionn/Live) (PPT，dofiles等)  
> <img style="width: 170px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会短书直播间-二维码170.png">

---
### 课程一览   


> 支持回看，所有课程可以随时购买观看。

| 专题 | 嘉宾    | 直播/回看视频    |
| --- | --- | --- |
| &#x2B50; **[Stata暑期班](https://gitee.com/arlionn/PX)** | 连玉君 <br> 江艇| [线上直播 9 天](https://www.lianxh.cn/news/e87cdb5c116d0.html) <br> 2020.7.28-8.7 |
| &#x2B55; **[Stata数据清洗](https://lxh.360dhf.cn/live/detail/4114)** | 游万海| [直播, 2 小时](https://www.lianxh.cn/news/f785de82434c1.html) <br> 2020.7.21 |
| **[效率分析-专题](https://gitee.com/arlionn/TE)** | 连玉君<br>鲁晓东<br>张&emsp;宁 | [视频-TFP-SFA-DEA](https://www.lianxh.cn/news/88426b2faeea8.html) <br> 已上线，3天 |
| **文本分析/爬虫** | 游万海<br>司继春 | [视频-文本分析与爬虫](https://www.lianxh.cn/news/88426b2faeea8.html) <br> 已上线，4天 |
| **[空间计量系列](https://lianxh.duanshu.com/#/brief/course/958fd224da8548e1ba7ff0740b536143)** | 范巧    | [空间全局模型](https://efves.duanshu.com/#/brief/course/ed1bc8fc5e7748c5aca7e2c39d28e20e), [空间权重矩阵](https://lianxh.duanshu.com/#/brief/course/94a5361647384a18852d28d1b9246362) <br> [空间动态面板](https://lianxh.duanshu.com/#/brief/course/f4e4b6b1e77c4ff88cecb685bbde07c3), [空间DID](https://lianxh.duanshu.com/#/brief/course/ff7dc9e0b82b40dab2047af0d01e96d0) |
| 研究设计 | 连玉君    | [我的特斯拉-实证研究设计](https://lianxh.duanshu.com/#/course/5ae82756cc1b478c872a63cbca4f0a5e)，[-幻灯片-](https://gitee.com/arlionn/Live/tree/master/%E6%88%91%E7%9A%84%E7%89%B9%E6%96%AF%E6%8B%89-%E5%AE%9E%E8%AF%81%E7%A0%94%E7%A9%B6%E8%AE%BE%E8%AE%A1-%E8%BF%9E%E7%8E%89%E5%90%9B)|
| 面板模型 | 连玉君    | [动态面板模型](https://efves.duanshu.com/#/brief/course/3c3ac06108594577a6e3112323d93f3e)，[-幻灯片-](https://quqi.gblhgk.com/s/880197/o7tDK5tHd0YOlYJl)   |
|     |     | [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) [免费公开课，2小时]  |


> Note: 部分课程的资料，PPT 等可以前往 [连享会-直播课](https://gitee.com/arlionn/Live) 主页查看，下载。

 <img style="width: 180px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/行走的人小.jpg">



&emsp;

> #### [连享会 - Stata 暑期班](https://gitee.com/arlionn/PX)     
> **线上直播 9 天**：2020.7.28-8.7  
> **主讲嘉宾**：连玉君 (中山大学) | 江艇 (中国人民大学)      
> **课程主页**：<https://gitee.com/arlionn/PX> | [微信版](https://mp.weixin.qq.com/s/_ypP4ol1_VnjOOBGZeAnoQ)  

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/2020Stata暑期班海报750.png "连享会：Stata暑期班，9天直播")

&emsp; 



&emsp;

---
>#### 关于我们

- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。[直播间](http://lianxh.duanshu.com) 有很多视频课程，可以随时观看。
- **你的颈椎还好吗？** 您将 [::连享会-主页::](https://www.lianxh.cn) 和 [::连享会-知乎专栏::](https://www.zhihu.com/people/arlionn/) 收藏起来，以便随时在电脑上查看往期推文。
- **公众号推文分类：** [计量专题](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=4&sn=0c34b12da7762c5cabc5527fa5a1ff7b) | [分类推文](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=2&sn=07017b31da626e2beab0332f5aa5f9e2) | [资源工具](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=3&sn=10c2cf37e172289644f03a4c3b5bd506)。推文分成  **内生性** | **空间计量** | **时序面板** | **结果输出** | **交乘调节** 五类，主流方法介绍一目了然：DID, RDD, IV, GMM, FE, Probit 等。
- **公众号关键词搜索/回复** 功能已经上线。大家可以在公众号左下角点击键盘图标，输入简要关键词，以便快速呈现历史推文，获取工具软件和数据下载。常见关键词：
  - `课程, 直播, 视频, 客服, 模型设定, 研究设计, `
  - `stata, plus，Profile, 手册, SJ, 外部命令, profile, mata, 绘图, 编程, 数据, 可视化`
  - `DID，RDD, PSM，IV，DID, DDD, 合成控制法，内生性, 事件研究` 
  - `交乘, 平方项, 缺失值, 离群值, 缩尾, R2, 乱码, 结果`
  - `Probit, Logit, tobit, MLE, GMM, DEA, Bootstrap, bs, MC, TFP`
  - `面板, 直击面板数据, 动态面板, VAR, 生存分析, 分位数`
  - `空间, 空间计量, 连老师, 直播, 爬虫, 文本, 正则, python`
  - `Markdown, Markdown幻灯片, marp, 工具, 软件, Sai2, gInk, Annotator, 手写批注`
  - `盈余管理, 特斯拉, 甲壳虫, 论文重现`
  - `易懂教程, 码云, 教程, 知乎`


---

![连享会主页  lianxh.cn](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会跑起来就有风400.png "连享会主页：lianxh.cn")



--- - --

> 连享会小程序：扫一扫，看推文，看视频……

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会小程序二维码180.png)

---

> 扫码加入连享会微信群，提问交流更方便

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会-学习交流微信群001-150.jpg)




